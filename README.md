# Tamagotchi

## Student: GRUBER AURORA

# Introduction

Tamagotchi is a famous toy from the end of the ’90s and the beginning of the new millennium. The goal of the game was to take care of a virtual pet, taking care of its basic needs, such as hunger and happiness. Of course, in the end, many kids just had tons of them on their consciences.
In Keil μVision, use the LANDTIGER board for implementing a basic Tamagotchi.

# Features
## STATUS
At the top of the screen, the STATUS of your virtual pet is always shown. There are three information in this status view:
1. The “Age” of your Tamagotchi (in hours:minutes:seconds);
2. The “Happiness” of your Tamagotchi (decreases every 5 seconds of one bar);
3. The “Satiety” of your Tamagotchi (decreases every 5 seconds nof one bar).
![puppet](./img/puppet.png)
## FOOD MENU
The FOOD MENU is always visible at the bottom of the screen. The option in the food menu are:
1. “Meal” to satisfy the hungry level of your Tamagotchi;
2. “Snack” to satisfy the happiness level of your Tamagotchi.
When selected, a RED square appears on the selected menu. Selecting a food or a snack triggers an animation with a sketch of the selected food and the Tamagotchi “eating” it.
The JOYSTICK is used to select the food options (LEFT/RIGHT for choosing the desired food and SELECT to choose it).
![feeding](./img/feeding.png)
## CUDDLES
Touching the Tamagotchi triggers an animation.
The cuddles increases the happiness level of one bar.
Cuddling is activated by pressing on the TouchScrenn present on the Landtiger.
Touching the meal/snack menu, as well as the status levels or the Tamagotchi age on top of the screen has no effect at all.
## SOUND
Each animation on your Tamagotchi includes a sound effect.
Sound/music is included to the following part of the project:
- Click of the Meal/Snack option.
- Eating (Meal/Snack) animation.
- Death / run away animation.
- Cuddles animation.
## VOLUME
The handling of the volume level is developed and a speaker icon with the following volume level is added. Volume level is sampled by reading the current analog value on the potentiometer every 50 ms.
## END 
The Tamagotchi leaves you if you don’t take care of it. If you reach the end of the Happiness or Satiety scale, the runaway sequence is triggered.
After it’s run away:
1. A message or icon is placed in the screen to signal that your Tamagotchi left you.
2. The age counter stops counting the age of your Tamagotchi.
3. A reset button replaces the food options.
By clicking the reset button, a complete reset of your Tamagotchi is triggered, with a new Tamagotchi appearing on your screen, with full Happiness and Satiety and with 00:00:00 as age.
For the Reset functionality, you simply must press the joystick (SELECT).
![escape](./img/escape.png)
![end](./img/end.png)
