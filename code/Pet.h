/* Includes ------------------------------------------------------------------*/
#include "LPC17xx.h"

/* Private function prototypes -----------------------------------------------*/
void drawPet(void);
void movePet(void);
int notFed(void);
int action(void);
void setFeeding(int val);
int notPlayed(void);
void end (void);
void box_r(void);
void box_l(void);
void clear_box_r(void);
void clear_box_l(void);
void box_reset(void);
void clear_box_reset(void);
void restart(void);
void mute(void);
void volume(void);
int playPet(void);
void cuddlePet(void);
static void Box(void);
static void Heart(int x);
int feedPet(void);
static void Star (int x);
static void Pet(void);
static void Body(int x, int y);
static void Hat(int x, int y);

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
