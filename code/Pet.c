#include "pet.h"
#include "GLCD/GLCD.h" 
#include "TouchPanel/TouchPanel.h"
#include "timer/timer.h"
#include "RIT.h"

enum color {
	none, black, red, grey, white, pink, lilla, brown
};
int flag = 2, xp = (MAX_X-60)/2, yp = 128, satiety = 5, happiness = 5, ys = 74, enable = 3;
extern int busy;
int pet[40][29] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //1
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, //2
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, //3
	0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, //4
	0, 0, 0, 0, 0, 0, 1, 3, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 1, 0, 0, 0, 0, 0, 0, //5
	0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 3, 3, 1, 0, 0, 0, 0, 0, //6
	0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 3, 3, 1, 0, 0, 0, 0, 0, //7
	0, 0, 0, 0, 0, 1, 3, 3, 1, 4, 1, 2, 2, 2, 2, 2, 2, 2, 1, 4, 1, 3, 3, 1, 1, 0, 0, 0, 0, //8
	0, 0, 0, 0, 0, 1, 3, 3, 1, 4, 4, 1, 1, 1, 1, 1, 1, 1, 4, 4, 1, 3, 3, 1, 2, 1, 0, 0, 0, //9
	0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 3, 3, 3, 1, 2, 1, 1, 0, 0, //10
	0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 1, 0, 1, 4, 4, 1, 0, //11
	0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 1, 4, 4, 1, 0, //12
	0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 1, 1, 0, 0, //13
	0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, 0, 0, //14
	0, 0, 0, 0, 0, 1, 3, 3, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 3, 3, 1, 0, 0, 0, 0, 0, //15
	0, 0, 0, 0, 0, 1, 3, 4, 4, 1, 4, 3, 3, 3, 3, 3, 3, 3, 4, 1, 4, 4, 3, 1, 0, 0, 0, 0, 0, //16
	0, 0, 0, 0, 0, 1, 3, 4, 4, 1, 4, 3, 3, 3, 3, 3, 3, 3, 4, 1, 4, 4, 3, 1, 0, 0, 0, 0, 0, //17
	0, 0, 0, 0, 1, 3, 3, 3, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 3, 3, 3, 1, 0, 0, 0, 0, //18
	0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, //19
	0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, //20
	0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, //21
	0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, //22
	0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, //23
	0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, //24
	0, 1, 3, 3, 1, 3, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 1, 1, 1, 3, 3, 3, 3, 1, 3, 3, 1, 0, //25
	0, 1, 3, 3, 1, 1, 2, 4, 2, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 3, 3, 1, 3, 3, 1, 0, //26
	1, 3, 3, 1, 1, 2, 4, 1, 4, 2, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 3, 3, 1, 3, 3, 1, //27
	1, 3, 3, 1, 1, 2, 1, 4, 1, 2, 1, 4, 4, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 1, 3, 1, 3, 3, 1, //28
	1, 3, 3, 1, 1, 4, 1, 3, 3, 1, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4, 3, 3, 4, 4, 1, 1, 3, 3, 1, //29
	1, 3, 3, 3, 1, 2, 1, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 4, 3, 4, 1, 1, 3, 3, 1, //30
	1, 3, 3, 3, 3, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 3, 3, 1, //31
	1, 3, 3, 3, 3, 1, 1, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 4, 1, 3, 3, 1, //32
	0, 1, 3, 3, 1, 4, 1, 4, 4, 4, 3, 4, 4, 3, 4, 4, 3, 4, 4, 3, 4, 4, 3, 3, 4, 1, 3, 1, 0, //33
	0, 1, 1, 1, 1, 1, 1, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 4, 1, 4, 1, 1, 0, //34
	0, 0, 1, 4, 1, 2, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, //35
	0, 0, 1, 4, 1, 4, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, //36
	0, 0, 0, 1, 4, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, 0, //37
	0, 0, 0, 0, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, 0, 0, //38
	0, 0, 0, 0, 0, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 0, 0, 0, 0, 0, //39
	0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0  //40
};
int sweet[20][20]= {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, 1, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 6, 4, 4, 5, 1, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 6, 4, 5, 5, 5, 1, 0, 
	0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 6, 5, 6, 5, 5, 6, 1, 
	0, 0, 0, 0, 0, 0, 1, 5, 5, 5, 5, 5, 6, 1, 6, 5, 6, 6, 6, 1,
	0, 0, 0, 0, 0, 1, 5, 5, 4, 4, 4, 5, 5, 6, 1, 6, 6, 6, 1, 1, 
	0, 0, 0, 0, 1, 5, 5, 4, 4, 4, 4, 4, 5, 5, 6, 1, 1, 1, 0, 0,
	0, 0, 0, 0, 1, 5, 4, 4, 4, 5, 5, 4, 5, 5, 6, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 5, 4, 4, 6, 5, 5, 5, 4, 5, 6, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 5, 4, 6, 6, 5, 5, 4, 4, 5, 6, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 5, 6, 6, 6, 5, 5, 5, 5, 5, 6, 1, 0, 0, 0, 0, 
	0, 0, 1, 1, 1, 5, 6, 6, 6, 6, 5, 5, 5, 6, 6, 1, 0, 0, 0, 0, 
	1, 1, 6, 6, 6, 1, 6, 6, 6, 6, 6, 6, 6, 6, 1, 0, 0, 0, 0, 0, 
	1, 5, 4, 4, 5, 6, 1, 6, 6, 6, 6, 6, 6, 1, 0, 0, 0, 0, 0, 0, 
	1, 5, 4, 5, 6, 5, 6, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 
	0, 1, 5, 5, 5, 6, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 1, 5, 6, 6, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 1, 5, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 	
};
int stick[13][7] = {
	0, 0, 1, 1, 1, 0, 0,
	0, 1, 2, 4, 2, 1, 0,
	1, 2, 4, 1, 4, 2, 1, 
	1, 2, 1, 0, 1, 2, 1,
	1, 4, 1, 0, 0, 1, 0, 
	1, 2, 1, 0, 0, 0, 0,
	1, 4, 1, 0, 0, 0, 0, 
	1, 2, 1, 0, 0, 0, 0, 
	1, 4, 1, 0, 0, 0, 0, 
	1, 2, 1, 0, 0, 0, 0, 
	1, 4, 1, 0, 0, 0, 0, 
	1, 2, 1, 0, 0, 0, 0,
  0, 1, 0, 0, 0, 0, 0	
};

int escape[37][29] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,  //1
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 1, 1, 1, 1, 1, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 2, 2, 1, 1, 3, 3, 3, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 2, 2, 2, 2, 2, 1, 3, 3, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 1, 2, 2, 2, 2, 2, 2, 2, 1, 3, 1, 0, 0, 0, 0, 0,  //5
	0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 1, 3, 3, 1, 2, 2, 2, 2, 2, 2, 2, 1, 3, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 1, 4, 4, 1, 1, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 1, 4, 4, 1, 0, 1, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 1, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, 0,  //10
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 4, 4, 3, 3, 3, 3, 4, 4, 3, 1, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 4, 4, 1, 4, 3, 3, 4, 4, 1, 4, 3, 1, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 4, 4, 1, 4, 3, 3, 4, 4, 1, 4, 3, 1, 0, 0, 0, //15
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 4, 4, 3, 3, 3, 3, 4, 4, 3, 3, 1, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 3, 1, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0,
	0, 0, 0, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 3, 3, 3, 3, 3, 3, 1, 0, 0, 
	0, 0, 1, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, //20
	0, 1, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 1, 3, 3, 3, 1, 3, 3, 1, 1, 1, 1, 1, 3, 3, 3, 1, 0,
	1, 7, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 4, 4, 4, 4, 4, 1, 1, 3, 1, 0,
	1, 7, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 3, 3, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 0, 
	1, 7, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 1, 1, 7, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 
	1, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 1, 7, 7, 7, 1, 4, 4, 4, 4, 3, 3, 3, 4, 4, 4, 4, 4, 1, //25
	1, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 3, 1, 7, 7, 1, 4, 4, 4, 3, 4, 4, 4, 3, 4, 4, 4, 3, 1, 
	1, 7, 7, 7, 7, 7, 1, 3, 3, 3, 3, 3, 3, 1, 1, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 1, 
	0, 1, 7, 7, 7, 7, 1, 3, 3, 3, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 
	0, 0, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 1, 4, 4, 3, 3, 4, 4, 4, 4, 3, 3, 4, 4, 4, 4, 4, 1,
	0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 1, 4, 3, 4, 4, 3, 4, 4, 3, 4, 4, 3, 4, 4, 3, 3, 1, //30
	0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 4, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 0, 0, //34
	0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 3, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0   //36
};
int heartL[6][7] = {
	0, 1, 1, 0, 1, 1, 0, 
	1, 4, 6, 1, 5, 5, 1, 
	1, 6, 5, 5, 5, 5, 1, 
	0, 1, 5, 5, 5, 1, 0, 
	0, 0, 1, 5, 1, 0, 0, 
	0, 0, 0, 1, 0, 0, 0
};
int heartS[5][5] = {
	0, 1, 0, 1, 0, 
	1, 4, 1, 6, 1,
  1, 6, 6, 6, 1,	
	0, 1, 6, 1, 0, 
	0, 0, 1, 0, 0, 
};
int volumeS[8][8] = {
	0, 0, 0, 0, 1, 0, 0, 0, 
	0, 0, 0, 1, 1, 0, 0, 0, 
	0, 0, 1, 1, 1, 0, 1, 0, 
	1, 1, 1, 1, 1, 0, 0, 1, 
	1, 1, 1, 1, 1, 0, 0, 1, 
	0, 0, 1, 1, 1, 0, 1, 0, 
	0, 0, 0, 1, 1, 0, 0, 0,
	0, 0, 0, 0, 1, 0, 0, 0
};
int muteS[8][8] = {
	2, 0, 0, 0, 1, 0, 0, 0, 
	0, 2, 0, 1, 1, 0, 0, 0, 
	0, 0, 2, 1, 1, 0, 0, 0, 
	1, 1, 1, 2, 1, 0, 0, 0, 
	1, 1, 1, 1, 2, 0, 0, 0, 
	0, 0, 1, 1, 1, 2, 0, 0, 
	0, 0, 0, 1, 1, 0, 2, 0,
	0, 0, 0, 0, 1, 0, 0, 2,
};
void drawPet(void){
	int x, y, i;
	LCD_Clear(Blue2);
	Box();
	y = 10;
	x = (MAX_X-124)/2;
	GUI_Text(x, y, (uint8_t *) "Age: 00:00:00", Black, Blue2);
	y = 38;
	GUI_Text(10, y, (uint8_t *) " Satiety", Black, Blue2);
	GUI_Text(MAX_X-74-18, y, (uint8_t *) " Happiness", Black, Blue2);
	y = 74;
	x = 10;
	for(i = 0; i<5; i++) {
		Heart(x);
		x+=16;
	}
	x = MAX_X-74-14;
	for(i = 0; i<5; i++) {
		Star(x);
		x+=16;
	}
	y = y + 50;
	Pet();
	return;
}

static void Box(void) {
	int y, x;
	for (y = (MAX_Y - 48); y < (MAX_Y) ; y++){
		LCD_Color(White, 0, MAX_X, y);
	}
	
	/*LCD_DrawLine(0, 0, MAX_X, 0, Black);
	LCD_DrawLine(0, 1, MAX_X, 1, Black);
	
	LCD_DrawLine((MAX_X - 1), 0, (MAX_X - 1), MAX_Y, Black);
	LCD_DrawLine((MAX_X - 2), 0, (MAX_X - 2), MAX_Y, Black);
	
	LCD_DrawLine(0, MAX_Y-1, MAX_X, MAX_Y-1, Black);
	LCD_DrawLine(0, MAX_Y-2, MAX_X, MAX_Y-2, Black);
	
	LCD_DrawLine(0, 0, 0, MAX_Y, Black);
	LCD_DrawLine(1, 0, 1, MAX_Y, Black);*/
	
	y = (MAX_Y - 50);
	LCD_Color(Black, 0, MAX_X, y);
	y += 1;
	LCD_Color(Black, 0, MAX_X, y);
	
	x = (MAX_X/2);
	LCD_DrawLine(x, (MAX_Y - 48), x, MAX_Y, Black);
	x -= 1;
	LCD_DrawLine(x, (MAX_Y - 48), x, MAX_Y, Black);
	
	x = ((MAX_X/2)-32-4-6)/2;
	GUI_Text(x, (MAX_Y-33), (uint8_t *) " Feed", Black, White);
	
	x = MAX_X/2 + (((MAX_X/2)-32-4-6)/2);
	GUI_Text(x, (MAX_Y-33), (uint8_t *) " Play", Black, White);
	
	return;
}

static void Heart(int x){
	int v_x[16] = {2, 4, 8, 10, 0, 6, 12, 0, 12, 0, 12, 2, 10, 4, 8, 6};
	int v_y[16] = {12, 12, 12, 12, 10, 10, 10, 8, 8, 6, 6, 4, 4, 2, 2, 0};
	int i=0, j, k;
	while (i<16) {
		j = v_y[i];
		k = v_x[i];
		while (v_y[i]==j) {
			if(k != v_x[i] && j < 12) {
				LCD_Square(x+k, ys-j, 2, Red);
			}
			else {
				LCD_Square(x+v_x[i], ys-j, 2, Black);
				i++;
			}
			k = k+2;
		}
	}
}
static void remHeart(int x) {
	int v_x[16] = {2, 4, 8, 10, 0, 6, 12, 0, 12, 0, 12, 2, 10, 4, 8, 6};
	int v_y[16] = {12, 12, 12, 12, 10, 10, 10, 8, 8, 6, 6, 4, 4, 2, 2, 0};
	int i=0, j, k;
	while (i<16) {
		j = v_y[i];
		k = v_x[i];
		while (v_y[i]==j) {
			if(k != v_x[i] && j < 12) {
				LCD_Square(x+k, ys-j, 2, Blue2);
			}
			else {
				LCD_Square(x+v_x[i], ys-j, 2, Blue2);
				i++;
			}
			k = k+2;
		}
	}
}
static void Star (int x) {
	int v_x[20] = {6, 4, 8, 0, 2, 10, 12, 2, 10, 4, 6, 8, 2, 4, 8, 10, 0, 2, 10, 12};
	int v_y[20] = {12, 10, 10, 8, 8, 8, 8, 6, 6, 4, 4, 4, 2, 2, 2, 2, 0, 0, 0, 0};
	int i = 0, j, k;
	while (i<20) {
		j = v_y[i];
		k = v_x[i];
		while (v_y[i]==j) {
			if(k != v_x[i] && j > 2) {
				LCD_Square(x+k, ys-j, 2, Yellow);
			}
			else {
				LCD_Square(x+v_x[i], ys-j, 2, Black);
				i++;
			}
			k = k + 2;
		}
	}
	return;
}
static void remStar (int x) {
	int v_x[20] = {6, 4, 8, 0, 2, 10, 12, 2, 10, 4, 6, 8, 2, 4, 8, 10, 0, 2, 10, 12};
	int v_y[20] = {12, 10, 10, 8, 8, 8, 8, 6, 6, 4, 4, 4, 2, 2, 2, 2, 0, 0, 0, 0};
	int i = 0, j, k, y = 74;
	while (i<20) {
		j = v_y[i];
		k = v_x[i];
		while (v_y[i]==j) {
			if(k != v_x[i] && j > 2) {
				LCD_Square(x+k, y-j, 2, Blue2);
			}
			else {
				LCD_Square(x+v_x[i], y-j, 2, Blue2);
				i++;
			}
			k = k + 2;
		}
	}
	return;
}
static void Pet() {
	int i, j;
	for (i = 0; i<40; i++){
		for (j = 0; j<29; j++){
			switch(pet[i][j]){
				case black:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Black);
					break;
				case red:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Red);
					break;
				case grey:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Grey);
					break;   
				case white:
					LCD_Square(xp+(j*2), yp+(i*2), 2, White);
					break;
				default:
					break;
			}
		}
	}
	return;
}

void movePet(){
	int t;
	int vx[24] = {8, 9, 19, 20, 7, 8, 9, 10, 18, 19, 20, 21, 7, 8, 9, 10, 18, 19, 20, 21, 8, 9, 19, 20};
	int vy[24] = {14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17};
	int c[24] = {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 3, 3, 1, 1, 3, 3, 1, 1, 1, 1, 1};
	if(flag){
		flag = !flag;
		t=0;
		while (t<24) {
			switch(c[t]){
					case black:
						LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Black);
						break;
					case grey:
						LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Grey);
						break;
					case white:
						LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, White);
						break;
			}
			t++;
		}
	}else {
		flag = !flag;
		t=23;
		while(t>-1){
			switch(pet[vy[t]][vx[t]]){
				case black:
					LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Black);
					break;
				case grey:
					LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Grey);
					break;
				case white:
					LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, White);
					break;
				}
			t--;
		}
	}
	return;
}


void cuddlePet() {
	static int x1, y1, count;
	int i, j, x = xp + 12, y = yp + 38;
	int aw[2][8] = {
		1, 2, 3, 4, 5, 2, 3, 4,
		0, 0, 0, 0, 0, 1, 1, 1, 
	};
	count++;
	switch(count){
		case 1:
		x1 = xp - 5;
		y1 = yp - 10;
		for(j = 0; j<8; j++){
			LCD_Square(x+(aw[0][j]*2), y+(aw[1][j]*2), 2, Lilla);
			LCD_Square(xp+58-12-(aw[0][j]*2), y+(aw[1][j]*2), 2, Lilla);
		}
					
		for (i = 0; i<6; i++)
		 for (j = 0; j<7; j++)
			switch(heartL[i][j]){
				case black:
					LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
					break;
				case pink:
						LCD_Square(x1+(j*2), y1+(i*2), 2, Pink);
					break;   
				case white:
					LCD_Square(x1+(j*2), y1+(i*2), 2, White);
					break;
				case lilla:
					LCD_Square(x1+(j*2), y1+(i*2), 2, Lilla);
					break;
				default:
					break;
			}
			break;
		case 2:
			for (i = 0; i<6; i++)
			 for (j = 0; j<7; j++)
						LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
			x1 += 2;
			y1 += 5;
			for (i = 0; i<5; i++)
			 for (j = 0; j<5; j++)
				switch(heartS[i][j]){
					case black:
						LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
						break;
					case white:
							LCD_Square(x1+(j*2), y1+(i*2), 2, White);
						break;
					case lilla:
						LCD_Square(x1+(j*2), y1+(i*2), 2, Lilla);
						break;
					default:
						break;
				}
				break;
			case 3:
				for (i = 0; i<5; i++)
				 for (j = 0; j<5; j++)
							LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
				x1 = xp - 5;
				y1 = yp - 10;
				for (i = 0; i<6; i++)
				 for (j = 0; j<7; j++)
					switch(heartL[i][j]){
						case black:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
							break;
						case pink:
								LCD_Square(x1+(j*2), y1+(i*2), 2, Pink);
							break;   
						case white:
							LCD_Square(x1+(j*2), y1+(i*2), 2, White);
							break;
						case lilla:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Lilla);
							break;
						default:
							break;
					}
						break;
			case 4:
				for (i = 0; i<6; i++)
					for (j = 0; j<7; j++)
						LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
				x1 += 2;
				y1 += 5;
				for (i = 0; i<5; i++)
				 for (j = 0; j<5; j++)
					switch(heartS[i][j]){
						case black:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
							break;
						case white:
								LCD_Square(x1+(j*2), y1+(i*2), 2, White);
							break;
						case lilla:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Lilla);
							break;
						default:
							break;
					}
				break;
			case 5:
				for (i = 0; i<5; i++)
				 for (j = 0; j<5; j++)
							LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
				for(j = 0; j<8; j++){
					LCD_Square(x+(aw[0][j]*2), y+(aw[1][j]*2), 2, Grey);
					LCD_Square(xp+58-12-(aw[0][j]*2), y+(aw[1][j]*2), 2, Grey);
				}
				if(happiness < 5) {
					i = MAX_X-74-14+((happiness)*16);
					happiness++;
					Star(i);
				}
				count = 0;
				enable = 3;
				busy = 0;
				break;
	}
}
int feedPet(){
	int vx[24] = {8, 9, 19, 20, 7, 8, 9, 10, 18, 19, 20, 21, 7, 8, 9, 10, 18, 19, 20, 21, 8, 9, 19, 20};
	int vy[24] = {14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17};
	int c[24] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 1, 4, 4, 1, 4, 1, 4};
	int i, j, t=0, x1=50, y1=MAX_Y-100;
	static int count  = 0, x;
	count++;
	switch(count) {
		case 1: //appare oggetto e sposta sguardo
			for (i = 0; i<20; i++){ //oggetto
				for (j = 0; j<20; j++){
					switch(sweet[i][j]){
						case black:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
							break;
						case pink:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Pink);
							break;
						case lilla:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Lilla);
							break;   
						case white:
							LCD_Square(x1+(j*2), y1+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			while (t<24) { //sguardo
				switch(c[t]){
					case black:
						LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Black);
						break;
					case white:
						LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, White);
						break;
				}
				t++;
			}
			break;
		case 2: //si sposta verso oggetto
			x = xp;
			xp = x1;
			for (i = 0; i<40; i++){ 
				for (j = 0; j<29; j++){
					LCD_Square(x+(j*2), yp+(i*2), 2, Blue2);
					switch(pet[i][j]){
						case black:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Black);
							break;
						case red:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Red);
							break;
						case grey:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Grey);
							break;   
						case white:
							LCD_Square(xp+(j*2), yp+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			break;
		case 3: //sparisce oggetto e mastica
			for (i = 0; i<5; i++) //bocca
				LCD_Square(xp+24+(i*2), yp+42, 2, Black);
			for (i = 0; i<20; i++)
				for (j = 0; j<20; j++)
					LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
		case 4: //reset
			for (i = 0; i<5; i++)
				if(i != 2)
					LCD_Square(xp+24+(i*2), yp+42, 2, Grey);
				else
					LCD_Square(xp+24+(i*2), yp+42, 2, Black);
				break;
		case 5: //torna al suo posto
			xp = x;
			for (i = 0; i<40; i++)
				for (j = 0; j<29; j++)
					LCD_Square(x1+(j*2), yp+(i*2), 2, Blue2);
			for (i = 0; i<40; i++){ 
				for (j = 0; j<29; j++){
					switch(pet[i][j]){
						case black:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Black);
							break;
						case red:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Red);
							break;
						case grey:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Grey);
							break;   
						case white:
							LCD_Square(xp+(j*2), yp+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			if (satiety<5) {
				i = 10 + ((satiety)*16);
				satiety++;
				Heart(i);
			}
			count = 0;
			enable_timer(1);
			enable_timer(2);
			enable = 3;
			busy = 0;
			break;
		}
	return satiety;
}

int notFed() {
	int x;
	satiety--;
	if(satiety >= 0)
		x = 10 + ((satiety)*16);
		remHeart(x);
	return satiety;
}

int playPet() {
	int vx[24] = {8, 9, 19, 20, 7, 8, 9, 10, 18, 19, 20, 21, 7, 8, 9, 10, 18, 19, 20, 21, 8, 9, 19, 20};
	int vy[24] = {14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17};
	int c[24] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 4, 4, 4, 1, 4, 4, 1, 4, 1};
	int i, j, t=0, x1 = MAX_X-64, y1 = MAX_Y-100;
	static int count  = 0, x;
	count ++;
	switch(count){
		case 1: //appare l'oggetto e totoro sposta lo sguardo
			for (i = 0; i<13; i++){ //oggetto
				for (j = 0; j<7; j++){
					switch(stick[i][j]){
						case black:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Black);
							break;
						case red:
							LCD_Square(x1+(j*2), y1+(i*2), 2, Red);
							break;   
						case white:
							LCD_Square(x1+(j*2), y1+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			while (t<24) { //sguardo
					switch(c[t]){
							case black:
								LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, Black);
								break;
							case white:
								LCD_Square(xp+(vx[t]*2), yp+(vy[t]*2), 2, White);
								break;
					}
					t++;
				}
			break;
		case 2: //si sposta verso oggetto
			x = xp;
			xp = x1-20;
			for (i = 0; i<40; i++){ 
				for (j = 0; j<29; j++){
					LCD_Square(x+(j*2), yp+(i*2), 2, Blue2);
					switch(pet[i][j]){
						case black:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Black);
							break;
						case red:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Red);
							break;
						case grey:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Grey);
							break;   
						case white:
							LCD_Square(xp+(j*2), yp+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			break;
		case 3: //sparisce oggetto e mastica
			for (i = 0; i<5; i++) //bocca
				LCD_Square(xp+24+(i*2), yp+42, 2, Black);
			for (i = 0; i<20; i++) //pulisce
				for (j = 0; j<20; j++)
					LCD_Square(x1+(j*2), y1+(i*2), 2, Blue2);
		break;
		case 4: //bocca normale
			for (i = 0; i<5; i++)
				if(i != 2)
					LCD_Square(xp+24+(i*2), yp+42, 2, Grey);
				else
					LCD_Square(xp+24+(i*2), yp+42, 2, Black);
					
				break;
		case 5: //sistema il resto
			xp = x;
			x = x1 - 20;
			for (i = 0; i<40; i++)
				for (j = 0; j<29; j++)
					LCD_Square(x+(j*2), yp+(i*2), 2, Blue2);
			for (i = 0; i<40; i++){ 
				for (j = 0; j<29; j++){
					switch(pet[i][j]){
						case black:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Black);
							break;
						case red:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Red);
							break;
						case grey:
							LCD_Square(xp+(j*2), yp+(i*2), 2, Grey);
							break;   
						case white:
							LCD_Square(xp+(j*2), yp+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			if(happiness < 5) {
				i = MAX_X-74-14+((happiness)*16);
				happiness++;
				Star(i);
			}
			count = 0;
			enable_timer(2);
			enable_timer(1);
			enable = 3;
			busy = 0;
			break;
		}
	return happiness;
}
int notPlayed() {
	int x;
	happiness--;
	if (happiness >= 0) {
		x = MAX_X-74-14 + ((happiness)*16);
		remStar(x);
	}
	return happiness;
}
void mute(){
	int i, j;
	for (i = 0; i<8; i++)
		for(j = 0; j<8; j++)
			switch(muteS[i][j]) {
				case black:
					LCD_Square(30+(j*2), 10+(i*2), 2, Black);
					break;
				case red:
					LCD_Square(30+(j*2), 10+(i*2), 2, Red);
				break;
				case 0:
					LCD_Square(30+(j*2), 10+(i*2), 2, Blue2);
				break;
			}
}
void volume() {
	int i, j;
	for (i = 0; i<8; i++)
		for(j = 0; j<8; j++)
			switch(volumeS[i][j]) {
				case black:
					LCD_Square(30+(j*2), 10+(i*2), 2, Black);
				break;
				case 0:
					LCD_Square(30+(j*2), 10+(i*2), 2, Blue2);
				break;
			}
}
void end(){
	static int count, x;
	int i, j, y = yp;
	count++;
	enable = 0;
	switch(count){
		case 1:
		x = xp + 30;
		for (i = 0; i<40; i++)
			for (j = 0; j<29; j++)
				switch(pet[i][j]){
				case black:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Blue2);
					break;
				case red:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Blue2);
					break;
				case grey:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Blue2);
					break;   
				case white:
					LCD_Square(xp+(j*2), yp+(i*2), 2, Blue2);
					break;
				default:
					break;
			}
		for (i = 0; i<37; i++){
			for (j = 0; j<29; j++){
				switch(escape[i][j]){
					case black:
						LCD_Square(x+(j*2), y+(i*2), 2, Black);
						break;
					case brown:
						LCD_Square(x+(j*2), y+(i*2), 2, Brown);
						break;
					case grey:
						LCD_Square(x+(j*2), y+(i*2), 2, Grey);
						break;
					case red:
						LCD_Square(x+(j*2), y+(i*2), 2, Red);
						break;
					case white:
						LCD_Square(x+(j*2), y+(i*2), 2, White);
						break;
					default:
						break;
				}
			}
		}
		break;
		case 2:
			for (i = 0; i<37; i++){
				for (j = 0; j<29; j++){
					LCD_Square(x+(j*2), y+(i*2), 2, Blue2);
				}
			}
			x+=40;
			for (i = 0; i<37; i++){
				for (j = 0; j<29; j++){
					switch(escape[i][j]){
						case black:
							LCD_Square(x+(j*2), y+(i*2), 2, Black);
							break;
						case brown:
							LCD_Square(x+(j*2), y+(i*2), 2, Brown);
							break;
						case grey:
							LCD_Square(x+(j*2), y+(i*2), 2, Grey);
							break;  
						case red:
							LCD_Square(x+(j*2), y+(i*2), 2, Red);
							break; 
						case white:
							LCD_Square(x+(j*2), y+(i*2), 2, White);
							break;
						default:
							break;
					}
				}
			}
			break;
			case 3:
				for (i = 0; i<37; i++){
					for (j = 0; j<29; j++){
						LCD_Square(x+(j*2), y+(i*2), 2, Blue2);
					}
				}
				disable_timer(0);
				GUI_Text((MAX_X-164)/2, (MAX_Y/2)-10, (uint8_t *) "Your pet is run away!", Red, Blue2);
				for (y = (MAX_Y - 48); y < (MAX_Y) ; y++){
					LCD_Color(White, 0, MAX_X, y);
				}
				count = 0;
				GUI_Text((MAX_X-50)/2, (MAX_Y-33), (uint8_t *) "RESET", Black, White);
				box_reset();
				GUI_Text((MAX_X-190)/2, (MAX_Y/2)+10, (uint8_t *) "Press RESET to play again", Black, Blue2);
				busy = 0;
				break;
	}
	return;
}
void box_r(){
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-1, MAX_X-1, MAX_Y-1, Red);
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-2, MAX_X-1, MAX_Y-2, Red);

	LCD_DrawLine((MAX_X/2)+1, MAX_Y-2, (MAX_X/2)+1, MAX_Y-48, Red);
	LCD_DrawLine((MAX_X/2)+2, MAX_Y-2, (MAX_X/2)+2, MAX_Y-48, Red);

	LCD_DrawLine((MAX_X/2)+1, MAX_Y-48, MAX_X-1, MAX_Y-48, Red);
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-47, MAX_X-1, MAX_Y-47, Red);

	LCD_DrawLine(MAX_X-1, MAX_Y-1, MAX_X-1, MAX_Y-48, Red);
	LCD_DrawLine(MAX_X-2, MAX_Y-1, MAX_X-2, MAX_Y-48, Red);	
}
void box_l(){
	LCD_DrawLine(0, MAX_Y-1, (MAX_X/2)-2, MAX_Y-1, Red);
	LCD_DrawLine(0, MAX_Y-2, (MAX_X/2)-2, MAX_Y-2, Red);

	LCD_DrawLine(0, MAX_Y-2, 0, MAX_Y-48, Red);
	LCD_DrawLine(1, MAX_Y-2, 1, MAX_Y-48, Red);

	LCD_DrawLine(2, MAX_Y-48, (MAX_X/2)-2, MAX_Y-48, Red);
	LCD_DrawLine(2, MAX_Y-47, (MAX_X/2)-2, MAX_Y-47, Red);

	LCD_DrawLine((MAX_X/2)-2, MAX_Y-47, (MAX_X/2)-2, MAX_Y-3, Red);
	LCD_DrawLine((MAX_X/2)-3, MAX_Y-47, (MAX_X/2)-3, MAX_Y-3, Red);		
}
void clear_box_r(){
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-1, MAX_X-1, MAX_Y-1, White);
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-2, MAX_X-1, MAX_Y-2, White);

	LCD_DrawLine((MAX_X/2)+1, MAX_Y-2, (MAX_X/2)+1, MAX_Y-48, White);
	LCD_DrawLine((MAX_X/2)+2, MAX_Y-2, (MAX_X/2)+2, MAX_Y-48, White);

	LCD_DrawLine((MAX_X/2)+1, MAX_Y-48, MAX_X-1, MAX_Y-48, White);
	LCD_DrawLine((MAX_X/2)+1, MAX_Y-47, MAX_X-1, MAX_Y-47, White);

	LCD_DrawLine(MAX_X-1, MAX_Y-3, MAX_X-1, MAX_Y-47, White);
	LCD_DrawLine(MAX_X-2, MAX_Y-3, MAX_X-2, MAX_Y-47, White);
}
void clear_box_l(){
	LCD_DrawLine(0, MAX_Y-1, (MAX_X/2)-2, MAX_Y-1, White);
	LCD_DrawLine(0, MAX_Y-2, (MAX_X/2)-2, MAX_Y-2, White);

	LCD_DrawLine(0, MAX_Y-3, 0, MAX_Y-48, White);
	LCD_DrawLine(1, MAX_Y-3, 1, MAX_Y-48, White);

	LCD_DrawLine(2, MAX_Y-48, (MAX_X/2)-2, MAX_Y-48, White);
	LCD_DrawLine(2, MAX_Y-47, (MAX_X/2)-2, MAX_Y-47, White);

	LCD_DrawLine((MAX_X/2)-2, MAX_Y-47, (MAX_X/2)-2, MAX_Y-3, White);
	LCD_DrawLine((MAX_X/2)-3, MAX_Y-47, (MAX_X/2)-3, MAX_Y-3, White);
}
void box_reset() {
	LCD_DrawLine(0, MAX_Y-1, MAX_X-1, MAX_Y-1, Red);
	LCD_DrawLine(0, MAX_Y-2, MAX_X-1, MAX_Y-2, Red);
	
	LCD_DrawLine(0, MAX_Y-1, 0, MAX_Y-47, Red);
	LCD_DrawLine(1, MAX_Y-1, 1, MAX_Y-47, Red);
	
	LCD_DrawLine(0, MAX_Y-48, MAX_X-1, MAX_Y-48, Red);
	LCD_DrawLine(0, MAX_Y-47, MAX_X-1, MAX_Y-47, Red);
	
	LCD_DrawLine(MAX_X-1, MAX_Y-1, MAX_X-1, MAX_Y-47, Red);
	LCD_DrawLine(MAX_X-2, MAX_Y-1, MAX_X-2, MAX_Y-47, Red);
}

void clear_box_reset() {
	LCD_DrawLine(0, MAX_Y-1, MAX_X-1, MAX_Y-1, White);
	LCD_DrawLine(0, MAX_Y-2, MAX_X-1, MAX_Y-2, White);
	
	LCD_DrawLine(0, MAX_Y-1, 0, MAX_Y-47, White);
	LCD_DrawLine(1, MAX_Y-1, 1, MAX_Y-47, White);
	
	LCD_DrawLine(0, MAX_Y-48, MAX_X-1, MAX_Y-48, White);
	LCD_DrawLine(0, MAX_Y-47, MAX_X-1, MAX_Y-47, White);
	
	LCD_DrawLine(MAX_X-1, MAX_Y-1, MAX_X-1, MAX_Y-47, White);
	LCD_DrawLine(MAX_X-2, MAX_Y-1, MAX_X-2, MAX_Y-47, White);
}
void restart() {
	enable = 3;
	happiness = 5;
	satiety = 5;
	disable_timer(0);
	disable_timer(1);
	disable_timer(2);
	reset_timer(0);
	reset_timer(1);
	reset_timer(2);
	drawPet();
	enable_timer(0);
	enable_timer(1);
	enable_timer(2);
}
