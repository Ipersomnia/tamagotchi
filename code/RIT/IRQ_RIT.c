/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_RIT.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    RIT.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "lpc17xx.h"
#include "RIT.h"
#include "../Pet.h"
#include "../GLCD/GLCD.h"
#include "../timer/timer.h"
#include "../TouchPanel/TouchPanel.h"

/******************************************************************************
** Function name:		RIT_IRQHandler
**
** Descriptions:		REPETITIVE INTERRUPT TIMER handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
/* k=1/f'*f/n  k=f/(f'*n) k=25MHz/(f'*45) */

//const int freqs[10]={4240,3779,3572,3367,3175,2834,2525,2249,2120, 0};
/* 
131Hz		k=4240 C3 0
147Hz		k=3779 D  1
165Hz		k=3367 E  2
175Hz		k=3175 F  3
196Hz		k=2834 G	4
220Hz		k=2525 A  5
247Hz		k=2249 H  6
262Hz		k=2120 C4 7
*/

const int freqs[12]={2120,1890,1786,1684,1592,1417,1263,1192,1125,1062, 945, 0};
/*
262Hz	k=2120	C4 0
294Hz	k=1890	D  1
311Hz k=1786  Es 2
330Hz	k=1684	E  3	
349Hz	k=1592	F  4	
392Hz	k=1417	G	 5	
440Hz	k=1263	A  6
466Hz k=1192  B  7
494Hz	k=1125	H  8	
523Hz	k=1062	C5 9
588Hz k=945  D 10
*/
#define N 9
int seq4beats[3][N]={{11, 3, 5, 9, -1, 5, 6, 5, -1},
										 {11, 0, 2, 4, -1, 4, 5, 2, -1},
										 {11, 7, -1, 10, -1, 9, -1, -1, -1}
										 };
/* contains sequence of notes from c4 to C5 */
/* position of c4 = 0 to C5 = 8 in the freq const vect ADC */
	
int play = 0;
/* current status of sound (on/off) */
int song = 0, busy = 0;										 
extern int enable, xp, yp;
void RIT_IRQHandler (void)
{					
	static int J_left = 0, J_right = 0, select = 0, position = 0, note, tick;	
	int frq;
	/* ADC management */
	ADC_start_conversion();
	
	if(!busy){
		if(enable)
			if(getDisplayPoint(&display, Read_Ads7846(), &matrix )){
				if(display.y > yp && display.y <  (yp + 80) && display.x > xp && display.x < xp+58){
					enable = 4;
				}
			}
		switch(enable){
		case 0:  //finito il gioco
		 if((LPC_GPIO1->FIOPIN & (1<<25)) == 0){
			select++;
			enable = 3;
			switch(select){
				case 1:
					disable_timer(3);
					reset_timer(3);
					frq = 7;
					init_timer(3, 0, 0, 3, freqs[frq]);
					enable_timer(3);
					position = 0;
					play = 1;
					note = N;
					clear_box_reset();
					restart();
					break;
				default:
					break;
			}
		}
		else {
			select = 0;
		}
		break;
		case 3: //game
		if((LPC_GPIO1->FIOPIN & (1<<27)) == 0){	
			/* Joytick left pressed p1.27*/
			/* Joytick right pressed p1.28*/
			J_left++;
			switch(J_left){
				case 1:
					clear_box_r();
					box_l();
					position = 1;
					break;
				default:
					break;
			}
		}
		else if((LPC_GPIO1->FIOPIN & (1<<28)) == 0){
				J_right++;
				switch(J_right){
				case 1:
					clear_box_l();
					box_r();
					position = 2;
					break;
				default:
					break;
			}
		}
		else if((LPC_GPIO1->FIOPIN & (1<<25)) == 0){ 
			select++;
			switch(select){
				case 1:
						switch(position){
						case 0: //none
							break;
						case 1: //timer1
							disable_timer(1);
							disable_timer(3);
							reset_timer(3);
							frq = 7;
							init_timer(3, 0, 0, 3, freqs[frq]);
							enable_timer(3);
							busy = 1;
							song = 0;
							note = 0;
							play = 1;	
							reset_timer(1);
							clear_box_l();
							position = 0;
							enable = 1;
						break;
						case 2: //timer 2	
							disable_timer(2);	
							disable_timer(3);
							reset_timer(3);
							frq = 7;
							init_timer(3, 0, 0, 3, freqs[frq]);
							enable_timer(3);
							busy = 1;
							song = 0;
							note = 0;
							play = 1;	
							reset_timer(2);
							clear_box_r();
							position = 0;
							enable = 2;
					}
					
					break;
				default:
					break;
			}
		}
		else {
			select = 0;
			J_right = 0;
			J_left = 0;
		}
		break;
		case 4:
			busy = 1;
			song = 1;
			note = 0;
			play = 1;
			cuddlePet();
			break;
		default:
			/*select = 0;
			J_right = 0;
			J_left = 0;*/
		break;
		}
	}
	if(play){
		tick++;
		if(tick%10 == 0){
			//per audio
			if(note == N){
				disable_timer(3);
				reset_timer(3);
				note = 0;
				play = 0;
				tick = 0;
			}
			else{
				if(frq != -1){
					disable_timer(3);
					reset_timer(3);
					frq = seq4beats[song][note++];
					init_timer(3, 0, 0, 3, freqs[frq]);
					enable_timer(3);
				}
			}
		}
	}
	disable_RIT();
	reset_RIT();
	enable_RIT();
	
  LPC_RIT->RICTRL |= 0x1;	/* clear interrupt flag */
  return;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
