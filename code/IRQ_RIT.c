/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_RIT.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    RIT.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "lpc17xx.h"
#include "RIT.h"
#include "Pet.h"
#include "GLCD/GLCD.h"
#include "timer/timer.h"
/******************************************************************************
** Function name:		RIT_IRQHandler
**
** Descriptions:		REPETITIVE INTERRUPT TIMER handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
extern int enable;
void RIT_IRQHandler (void)
{					
	static int J_left = 0, J_right = 0, select = 0, position = 0;
	if(enable){
		if((LPC_GPIO1->FIOPIN & (1<<27)) == 0){	
			/* Joytick left pressed p1.27*/
			/* Joytick right pressed p1.28*/
			J_left++;
			switch(J_left){
				case 1:
					clear_box_r();
					box_l();
					position = 1;
					break;
				default:
					break;
			}
		}
		else if((LPC_GPIO1->FIOPIN & (1<<28)) == 0){
				J_right++;
				switch(J_right){
				case 1:
					clear_box_l();
					box_r();
					position = 2;
					break;
				default:
					break;
			}
		}
		else if((LPC_GPIO1->FIOPIN & (1<<25)) == 0){ 
			select++;
			disable_RIT();
			switch(select){
				case 1:
   					switch(position){
						case 0: //none
							break;
						case 1: //timer1	
							reset_timer(1);
							disable_timer(2);
							setFeeding(1);
							clear_box_l();
							position = 0;
							enable_timer(3);
						break;
						case 2: //timer 2		
							reset_timer(2);
							disable_timer(1);
							setFeeding(2);
							clear_box_r();
							position = 0;
							enable_timer(3);
					}
					break;
				default:
					break;
			}
		}
		else {
			select = 0;
			J_right = 0;
			J_left = 0;
		}
	}
	else { //finito il gioco
		 if((LPC_GPIO1->FIOPIN & (1<<25)) == 0){ //ho messo down perch� select non funge (25)
			select++;
			disable_RIT();
			switch(select){
				case 1:
					position = 0;
					clear_box_reset();
					restart();
					break;
				default:
					break;
			}
		}
		else {
			select = 0;
		}
	}
	reset_RIT();
	enable_RIT();
  LPC_RIT->RICTRL |= 0x1;	/* clear interrupt flag */
	
  return;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
