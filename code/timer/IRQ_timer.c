/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_timer.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    timer.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include <string.h>
#include "lpc17xx.h"
#include "timer.h"
#include "../GLCD/GLCD.h" 
#include "../TouchPanel/TouchPanel.h"
#include "../Pet.h"
#include "../RIT.h"
#include "../adc/adc.h"
/******************************************************************************
** Function name:		Timer0_IRQHandler
**
** Descriptions:		Timer/Counter 0 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/


extern int volumeLevel;
extern int enable, song, play;										 
uint16_t SinTable[45] =                                       /*                      */
{
    410, 467, 523, 576, 627, 673, 714, 749, 778,
    799, 813, 819, 817, 807, 789, 764, 732, 694, 
    650, 602, 550, 495, 438, 381, 324, 270, 217,
    169, 125, 87 , 55 , 30 , 12 , 2  , 0  , 6  ,   
    20 , 41 , 70 , 105, 146, 193, 243, 297, 353
};

static int count = 0;

void TIMER0_IRQHandler (void)
{
	int hh = 0, mm = 0, ss = 0, x = ((MAX_X-124)/2)+40, tmp;
	char time_in_char[9] = "";
	/* Match register 0 interrupt service routine */
 	if (LPC_TIM0->IR & 01){
		if(enable){
			count++;
			if(count % 2 == 0){
				tmp = count/2;
				hh = tmp/(3600); //3600*2
				mm = tmp/(60);
				ss = tmp%60; //60*2
				sprintf(time_in_char,"%02d:%02d:%02d", hh, mm, ss);
				GUI_Text(x, 10, (uint8_t *) time_in_char, Black, Blue2);
			}
		}
		LPC_TIM0->IR = 1;			/* clear interrupt flag */
	}
	/* Match register 1 interrupt service routine */
	else if(LPC_TIM0->IR & 02){
		switch(enable) {
			case 0:	
				end();
			  count = 0;
				break;
			case 1:
				feedPet();
				break;
			case 2:
				playPet();
				break;
			case 3:
				if(count%2 == 0)
					movePet();
				break;
			case 4:
				if(count%2 == 0)
					movePet();
				cuddlePet();
				break;
			default:
				break;
		}			
		LPC_TIM0->IR =  2 ;			/* clear interrupt flag */	
	}
	else if(LPC_TIM0->IR & 4){
		LPC_TIM0->IR =  4 ;			/* clear interrupt flag */	
	}
	/* Match register 3 interrupt service routine */
	else if(LPC_TIM0->IR & 8){
		LPC_TIM0->IR =  8 ;			/* clear interrupt flag */	
	}
  return;
}


/******************************************************************************
** Function name:		Timer1_IRQHandler
**
** Descriptions:		Timer/Counter 1 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
void TIMER1_IRQHandler (void) //timer feed
{
	int tmp;
	/* Match register 0 interrupt service routine */
	if (LPC_TIM1->IR & 01){
		tmp = notFed();
		if(!tmp) {
			song = 2;
			play = 1;
			disable_timer(1);
			disable_timer(2);
			enable = 0;
		}
		LPC_TIM1->IR = 1;			/* clear interrupt flag */
	}
	/* Match register 1 interrupt service routine */
	else if(LPC_TIM1->IR & 02)
  {		
		LPC_TIM1->IR =  2 ;			/* clear interrupt flag */	
	}
	else if(LPC_TIM1->IR & 4){
		LPC_TIM1->IR =  4 ;			/* clear interrupt flag */	
	}
	/* Match register 3 interrupt service routine */
	else if(LPC_TIM1->IR & 8)
  {
		LPC_TIM1->IR =  8 ;			/* clear interrupt flag */	
	}
  return;
}

/******************************************************************************
** Function name:		Timer2_IRQHandler
**
** Descriptions:		Timer/Counter 2 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
void TIMER2_IRQHandler (void)
{
	int tmp;
	/* Match register 0 interrupt service routine */
	if (LPC_TIM2->IR & 01){
		tmp = notPlayed();
		if (!tmp) {
			song = 2;
			play = 1;
			disable_timer(1);
			disable_timer(2);
			enable = 0;
		}
		LPC_TIM2->IR = 1;			/* clear interrupt flag */
	}
	/* Match register 1 interrupt service routine */
	else if(LPC_TIM2->IR & 02)
  {
		LPC_TIM2->IR =  2 ;			/* clear interrupt flag */	
	}
	else if(LPC_TIM2->IR & 4){
		LPC_TIM2->IR =  4 ;			/* clear interrupt flag */	
	}
	/* Match register 3 interrupt service routine */
	else if(LPC_TIM2->IR & 8)
  {
	 
		LPC_TIM2->IR =  8 ;			/* clear interrupt flag */	
	}
  return;
}

void TIMER3_IRQHandler (void)
{
	static int ticks=0;
	float tmp;
	/* Match register 0 interrupt service routine */
	if (LPC_TIM3->IR & 01){
		/* DAC management */
		LPC_DAC->DACR = (SinTable[ticks]*volumeLevel/10)<<6;//((volumeLevel)*SinTable[ticks])<<6;
		ticks++;
		if(ticks==45) ticks=0;
		LPC_TIM3->IR = 1;			/* clear interrupt flag */
	}
	/* Match register 1 interrupt service routine */
	else if(LPC_TIM3->IR & 02){
		LPC_TIM3->IR =  2 ;			/* clear interrupt flag */	
	}
	else if(LPC_TIM3->IR & 4){
		LPC_TIM3->IR =  4 ;			/* clear interrupt flag */	
	}
	/* Match register 3 interrupt service routine */
	else if(LPC_TIM3->IR & 8)
  {
		LPC_TIM3->IR =  8 ;			/* clear interrupt flag */	
	}
  return;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
